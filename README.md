# AUTH Server with Go echo framework

## Pre-installation
You have to install all of this package before running.
- `go get -u -v golang.org/x/lint/golint`
- `go get -u -v github.com/labstack/echo`
- `go get -u -v github.com/jinzhu/gorm`
- `go get -u -v github.com/tkanos/gonfig`
- `go get -u -v github.com/swaggo/echo-swagger`
- `go get -u -v github.com/sirupsen/logrus`


## Configurations
Create or copy from `config/config.json.example` to `config/config.json`. Configuration used gonfig. All configs are declared in `config/config.json`

## Architecture
| Folder | Details |
| --- | ---|
| api | Holds the api endpoints |
| db | Database Initializer and DB manager |
| route | router setup |
| model | Models|

## Swagger
### Usage
1. Add comments to your API source code, [See Declarative Comments Format](https://github.com/swaggo/swag#declarative-comments-format).
2. Download [Swag](https://github.com/swaggo/swag) for Go by using:
```sh
$ go get github.com/swaggo/swag/cmd/swag
```
3. Run the [Swag](https://github.com/swaggo/swag) in your Go project root folder which contains `main.go` file, [Swag](https://github.com/swaggo/swag) will parse comments and generate required files(`docs` folder and `docs/doc.go`). If you cant run swag, export folder bin to your path `export PATH=$PATH:$(go env GOPATH)/bin`
```sh_ "github.com/swaggo/echo-swagger/v2/example/docs"
$ swag init -g route/router.go
```

## Run 
`go run server.go`


