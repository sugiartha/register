package main

import (
	"register/db"
	"register/route"
)

func main() {
	db.Init()
	e := route.Init()

	e.Logger.Fatal(e.Start(":1232"))
}
