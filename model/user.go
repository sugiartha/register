package model

type User struct {
	ID       int    `gorm:"column:id" json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
}
